<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;

use App\Http\Requests;
class PhotoController extends Controller
{
    //
     public function index()
     {


        $photos = Photo::all();


        return view('admin.photo.index', compact('photos'));

      }


    public function create()
    {


        return view('admin.photo.create');


    }

    // Store a newly 
    public function store(Request $request)
    {
        //

        Photo::create($request->all());


        return redirect('/admin/Photo');


    }


}
