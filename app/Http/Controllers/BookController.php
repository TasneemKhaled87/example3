<?php

namespace App\Http\Controllers;


use App\Book;
use Illuminate\Http\Request;

use App\Http\Requests;
class BookController extends Controller
{
    // //Display a listing of the resource.
    
    public function index()
    {
        
        $books = Book::all();


        return view('admin.books.index', compact('books'));


    }

     // Store a newly 
    
    public function store(Request $request)
    {
        

        Category::create($request->all());


        return redirect('/admin/books');


    }

    // Display the specified resource.
     
    public function show($id)
    {
        
    }

     //Show the form for editing .
     
    public function edit($id)
    {
        

        $book = Book::findOrFail($id);


        return view('admin.books.edit', compact('book'));



    }

    //Update the specified resource
    public function update(Request $request, $id)
    {
        //

        $category = Book::findOrFail($id);

        $category->update($request->all());

        return redirect('/admin/books');


    }

    //Remove the specified resource.
     
    public function destroy($id)
    {
        //

        Category::findOrFail($id)->delete();


        return redirect('/admin/books');



    }
}
