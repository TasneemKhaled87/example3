<?php

namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
class CategoryController extends Controller
{
    //

    //Display a listing of the resource.
    
    public function index()
    {
        //


        $categories = Category::all();


        return view('admin.categories.index', compact('categories'));


    }

     // Store a newly category
    
    public function store(Request $request)
    {
        //

        Category::create($request->all());


        return redirect('/admin/categories');


    }

    // Display the specified resource.
     
    public function show($id)
    {
        //
    }

     //Show the form for editing .
     
    public function edit($id)
    {
        //

        $category = Category::findOrFail($id);


        return view('admin.categories.edit', compact('category'));



    }

    //Update the specified resource
    public function update(Request $request, $id)
    {
        //

        $category = Category::findOrFail($id);

        $category->update($request->all());

        return redirect('/admin/categories');


    }

    //Remove the specified resource.
     
    public function destroy($id)
    {
        //

        Category::findOrFail($id)->delete();


        return redirect('/admin/categories');



    }
}
