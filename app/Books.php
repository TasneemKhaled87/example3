<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{


     protected $fillable = [

        'title',
        'description',
        'photo_id',
        'author_id',
        'category_id',
        'user_id',
        'published_at'
        



    ];
	//this book related to the category

    public function category()
    {
        return $this->belongsTo('App\Category');
    }


//this book related to the user
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //this book related to author
    public function author()
    {
        return $this->belongsTo('App\Author');
    }

    //this book related to photo
    public function photos()
    {
        return $this->belongsTo('App\Photo');
    }
}

