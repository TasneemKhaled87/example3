<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    //related to the books
    public function books()
    {
        return $this->hasMany('App\Book');
    }
}
