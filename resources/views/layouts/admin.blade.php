<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin</title>


    
  
	<title>Home page</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

    
</head>


<body id="admin-page">

<body>
	<hr/>
	<div class="container">
	
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">My Profile</a>
			<a class="navbar-brand" href="/">Home</a>
			<a class="navbar-brand" href="/">Category</a>
			<a class="navbar-brand" href="/">Book</a>
			
			
        </div>
        </nav>
        <!-- /.navbar-header -->
		
		  <li>
              <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
          </li>

          <!--category -->
          <hr>

          <li>
              <a href="#">
                <i class="fa fa-wrench fa-fw"></i>Categories<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="/categories">All Categories</a>
                            </li>
                            <li>
                                <a href="/categories/create">Create Category</a>
                            </li>

                        </ul>
                        
                    </li>
                    </li>

              <li>
              <a href="#"><i class="fa fa-wrench fa-fw">Books</i><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="/books">All Books</a>
                            </li>

                            <li>
                                <a href="/books/create">Add Books</a>
                            </li>

                        </ul>
                        
            </li>
  
         

</div>

	@yield('body')
		
	</div>
</body>
</html>



