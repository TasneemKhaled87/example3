@extends('layouts.admin')



@section('content')


    <h1>Books</h1>


    <table class="table table-striped">
       <thead>
         <tr>
             <th>Id</th>
             <th>Title</th>
             <th>Author</th>
             <th>Description</th>
             <th>Photo_id</th>
             <th>Category_id</th>
             <th>User_id</th>
             <th>published_at</th>
             <th>Created at</th>
             <th>Update</th>
        </thead>
        <tbody>

        @if($books)


            @foreach($books as $book)

          <tr>
              <td><img height="50" src="{{$book->photo ? $book->photo->file : 'http://..../400x400' }} " alt=""></td>
              <td>{{$book->id}}</td>
              <td>{{$book->author->name}}</a></td>
              <td>{{$book->category ? $book->category->name : 'Uncategorized'}}</td>
              <td>{{$book->title}}</td>
              <td>{{$book->published_at->diffForhumans()}}</td>
              <td>{{$book->created_at->diffForhumans()}}</td>
              <td>{{$book->updated_at->diffForhumans()}}</td>
              <td><a href="{{action('BookController@edit', $book['id'])}}" class="btn btn-warning">Edit</a></td>
              <td>
                 <form action="{{action('BookController@destroy', $book['id'])}}" method="post">
                    {{csrf_field()}}
                     <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger" type="submit">Delete</button>
                   </form>
              </td>

          </tr>

            @endforeach

            @endif

       </tbody>
     </table>



    <div class="row">
        <div class="col-sm-6 col-sm-offset-5">

            {{$books->render()}}

        </div>
    </div>



@stop
