<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //


    protected $fillable = [
          'name',
          'extension',
            'url'

    ];


    public function books()
    {
        return $this->belongsTo('App\Book');
    }
}
