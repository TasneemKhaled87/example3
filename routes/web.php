<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Route::get('about', function () {

   // return view('about');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//crud
Route::resource('/crud', 'CrudsController');

//category

Route::resource('admin/categories', 'CategoryController',['names'=>[


        'index'=>'admin.categories.index',
        'create'=>'admin.categories.create',
        'store'=>'admin.categories.store',
        'edit'=>'admin.categories.edit'


    ]]);


//books


  Route::resource('admin/books', 'BookController',['names'=>[

        'index'=>'admin.books.index',
        'create'=>'admin.books.create',
        'store'=>'admin.books.store',
        'edit'=>'admin.books.edit'





    ]]);


//photos

    Route::resource('admin/photo', 'PhotoController',['names'=>[

        'index'=>'admin.photo.index',
        'create'=>'admin.photo.create',
        'store'=>'admin.photo.store',
        'edit'=>'admin.photo.edit'




    ]]);